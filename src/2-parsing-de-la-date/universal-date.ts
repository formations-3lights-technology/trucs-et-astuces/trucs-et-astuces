/*
01 Mai,1943 -> 1943-05-01
09/14/79    -> 1979-09-14
Mai 01,1949 -> 1949-05-01
01 MAI 43   -> 1943-05-01 => Discussion avec le métier
08 Août 08  -> 2008-08-08 => Discussion avec le métier
'02/03/2010 -> 2010-03-02
01/01/01    -> 2001-01-01
03/18       -> 2018-03-01

03Q320      -> ?
031/218     -> ?
M18         -> 30 June 2018
M8          -> 30 Juin 2018
-3OFM62018  -> 3 jours ouvrés avant fin de mois juin 2018
+3TARGET 	  -> aujourd'hui 00:00:00 + 3 jours
+3OTARGET   -> aujourd'hui 00:00:00 + 3 jours ouvrés
 */

import { isMatch, parse } from 'date-fns'
import { fr }             from 'date-fns/locale'

export class UnsupportedDateFormat extends Error {
}

export class UniversalDate {
	private readonly options                    = { locale: fr }
	private readonly availableFormats: string[] = [
		'dd MMMM,yyyy',
		'dd MMMM yy',
		'MM/dd/yy',
		'MMMM dd,yyyy',
		'dd/MM/yyyy',
		'MM/yy'
	]

	constructor(private _date: string) {
		const sanitizedDate  = this.sanitize(this._date)
		const matchedFormats = this.findMatchedFormats(sanitizedDate)
		if (matchedFormats.length === 0)
			throw new UnsupportedDateFormat(this._date)
	}

	get value(): Date {
		const sanitizedDate   = this.sanitize(this._date)
		const [matchedFormat] = this.findMatchedFormats(sanitizedDate)
		return parse(sanitizedDate, matchedFormat, new Date(), this.options)
	}

	private sanitize(string: string): string {
		return string.replace(/^\'/g, '').toLowerCase()
	}

	private findMatchedFormats(sanitizedDate: string): string[] {
		return this.availableFormats
			.filter(format => isMatch(sanitizedDate, format, this.options))
	}
}


