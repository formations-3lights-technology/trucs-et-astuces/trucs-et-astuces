import { UniversalDate } from './universal-date'

const date1 = new UniversalDate('04 Juin,2004')
console.log(date1.value)

const date2 = new UniversalDate('12/19')
console.log(date2.value)

const date3 = new UniversalDate('\'15/11/2017')
console.log(date3.value)
