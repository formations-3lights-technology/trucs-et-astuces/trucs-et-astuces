import assert                                   from 'assert'
import { expect }                               from 'chai'
import { UniversalDate, UnsupportedDateFormat } from './universal-date'

describe('Universal Date', () => {
	 it('Parse dd MMMM,yyyy format (eg. 01 Mai,1943)', () => {
			const dateToParse = '01 Mai,1943'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('1943-05-01T00:00:00+0200'))
	 })

	 it('Parse MM/dd/yy format (eg. 09/14/79)', () => {
			const dateToParse = '09/14/79'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('1979-09-14T00:00:00+0200'))
	 })

	 it('Parse MMMM dd,yyyy format (eg. Mai 01,1949)', () => {
			const dateToParse = 'Mai 01,1949'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('1949-05-01T00:00:00+0100'))
	 })

	 it('Parse dd MMMM yy format (eg. 08 Août 08)', () => {
			const dateToParse = '08 Août 08'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('2008-08-08T00:00:00+0200'))
	 })

	 it('Parse excel format (eg. \'02/03/2010)', () => {
			const dateToParse = '\'02/03/2010'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('2010-03-02T00:00:00+0100'))
	 })

	 it('Parse dd/MM/yy format (eg. 01/01/01)', () => {
			const dateToParse = '01/01/01'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('2001-01-01T00:00:00+0100'))
	 })

	 it('Parse MM/yy format (eg. 03/18)', () => {
			const dateToParse = '03/18'

			const result = new UniversalDate(dateToParse)

			expect(result.value).to.eql(new Date('2018-03-01T00:00:00+0100'))
	 })

	 it('Unsupported date format', () => {
			const dateToParse = 'Unsupported date format'

			try {
				 new UniversalDate(dateToParse)
				 assert.fail()
			} catch (e) {
				 expect(e.message).to.eql('Unsupported date format')
				 expect(e).to.instanceOf(UnsupportedDateFormat)
			}
	 })
})
