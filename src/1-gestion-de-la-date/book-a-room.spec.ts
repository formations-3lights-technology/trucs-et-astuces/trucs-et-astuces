import assert                                                               from 'assert'
import { expect }                                                           from 'chai'
import { BookARoom, DateProvider, ReservationStartDateIsBeforeCurrentDate } from './book-a-room'

export class InMemoryDateProvider implements DateProvider {
  constructor(private currentDate: string) {
  }

  now(): Date {
    return new Date(this.currentDate)
  }
}

describe('Book a room', () => {
  let dateProvider: InMemoryDateProvider
  let bookARoom: BookARoom

  beforeEach(() => {
    dateProvider = new InMemoryDateProvider('2020-08-30')
    bookARoom = new BookARoom(dateProvider)
  })

  it('Everything is ok', () => {
    const reservationStartDate = new Date('2020-08-31')
    const result               = bookARoom.execute(reservationStartDate)

    expect(result).to.be.true

  })

  describe('Something is wrong', () => {
    it('Informs that the start date of the reservation is before the date of the current day', () => {
      // @ts-ignore
      dateProvider.now.returns(new Date('2020-09-02'))
      const reservationStartDate = new Date('2020-09-01')

      try {
        bookARoom.execute(reservationStartDate)
        assert.fail()
      } catch (e) {
        expect(e).to.instanceOf(ReservationStartDateIsBeforeCurrentDate)
      }
    })
  })
})
