import { BookARoom, DateProvider } from './book-a-room'

class RealDateProvider implements DateProvider {
  now(): Date {
    return new Date()
  }
}

const dateProvider = new RealDateProvider()

const bookARoom = new BookARoom(dateProvider)

try {
  console.log(bookARoom.execute(new Date('2020-08-29')))
} catch (e) {
  console.log(e.message)
}
