export class ReservationStartDateIsBeforeCurrentDate extends Error {
  constructor() {
    super('Reservation start date is before current date');
  }
}

export class BookARoom {
  constructor(private dateProvider: DateProvider) {
  }

  execute(startDate: Date): boolean {
    if (startDate < this.dateProvider.now())
      throw new ReservationStartDateIsBeforeCurrentDate()
    return true
  }
}

export interface DateProvider {
  now(): Date
}
